#!/usr/bin/python3

from bs4 import BeautifulSoup
import requests
import json


def print_all_headlines_for_uri(stories,uri):
    print(*stories[uri], sep=", ")

def count_all_headlines(stories):
    total=0
    for k in stories.keys():
        total+=len(stories[k])
    return total


def read_stories_from_file():
    # read in the existing json as a dict
    stories={}
    with open('1.json') as f:
        stories = json.load(f)
    return stories


def build_html_page(stories):




def main():
    # we only care about the front page.
    url = "https://www.smh.com.au"
    page = requests.get(url,timeout=5)
    content = BeautifulSoup(page.content, "lxml")
    h3s = content.find_all("h3")

    # links/URIs are the key since they never change.
    # stories have ONE canonical link, the headlines change and that's what we're tracking
    # link: list(content.find("h3").children)[1]['href']
    # text: list(content.find("h3").children)[1].text

    stories = read_stories_from_file()
    print("read in " + str(len(stories)) + " unique uris")
    print("read in " + str(count_all_headlines(stories)) + " unique headlines")

    total=0
    known_uris=0
    new_uris=0
    known_headlines=0
    new_headlines=0
    errors=0

    for h3 in h3s:
        try:
            uri = list(h3.children)[1]['href'] 
            clean_uri = uri.split('?')[0].split('#')[0]
            headline = list(h3.children)[1].text
            total+=1
        except:
            #print (list(h3.children))
            errors+=1
        # two cases: uri exists or it doesnt
        if (clean_uri not in stories):
            new_uris+=1
            print("new uri: " + clean_uri + " - " + headline)
            stories[clean_uri] = []
            stories[clean_uri].append(headline)
        else: # we already know the uri
            known_uris+=1
            # check if the headline already exists
            if (headline not in stories[clean_uri]):
                new_headlines+=1
                print ("new headline: " + clean_uri + " - " + headline)
                print_all_headlines_for_uri(stories, clean_uri)
                stories[clean_uri].append(headline)
            else:
                known_headlines+=1
                #print ('headline already known')
    # save the dict as json
    with open('1.json', 'w') as f:
        json.dump(stories,f)

    print("=================")
    print ("known_uris: " + str(known_uris))
    print ("new uris: " + str(new_uris))
    print ("known headlines: " + str(known_headlines))
    print ("new headlines: " + str(new_headlines))
    print("errors: " + str(errors))

    """
    for k in stories:
        print (k)
        print (stories[k])
    """



if __name__ == '__main__':
    main()
